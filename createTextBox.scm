; schemeee is craaazy :D

;main function
(define 
	(
	;function-name
	batch-create-img
	
	;functionParameters	
	inText
	inFont
	inFontSize
	inTextColor 
	)
	
	;the magick
	(let*
      		(
        		; define our local variables
        		; create a new image:
        		(theImageWidth  10)
        		(theImageHeight 10)
        		(theImage)
        	
			(theImage
                		(car
                      			(gimp-image-new
                        		theImageWidth
                        		theImageHeight
                        		RGB
                      			)
                  		)
        		)

        		(theText)             ;a declaration for the text
        		(theBuffer)           ;create a new layer for the image
        		(theLayer
                  		(car
                      			(gimp-layer-new
                        		theImage
                        		theImageWidth
                        		theImageHeight
                        		RGB-IMAGE
                        		"layer 1"
                        		100
                        		LAYER-MODE-NORMAL
                      			)
                  		)
        		)
      		) ;end of our local variables
      

		(gimp-image-insert-layer theImage theLayer 0 0)
      		(gimp-context-set-background '(255 255 255) )
      		(gimp-context-set-foreground inTextColor)
      		(gimp-drawable-fill theLayer BACKGROUND-FILL)
      		(set! theText
        		(car
                		(gimp-text-fontname
                        	 theImage theLayer
                        	 0 0
                        	 inText
                        	 0
                        	 TRUE
                        	 inFontSize PIXELS
                        	 inFont
				)
               		)
        	)
      		(set! theImageWidth   (car (gimp-drawable-width  theText) ) )
      		(set! theImageHeight  (car (gimp-drawable-height theText) ) )
      		(set! theBuffer (* theImageHeight (/ inBufferAmount 100) ) )
      		(set! theImageHeight (+ theImageHeight theBuffer theBuffer) )
      		(set! theImageWidth  (+ theImageWidth  theBuffer theBuffer) )
      		(gimp-image-resize theImage theImageWidth theImageHeight 0 0)
      		(gimp-layer-resize theLayer theImageWidth theImageHeight 0 0)
      		(gimp-layer-set-offsets theText theBuffer theBuffer)
      		(gimp-floating-sel-to-layer theText)
      		(gimp-display-new theImage)
      		(list theImage theLayer theText)
   	 ) ;end of expression
) ;end of function



;Register the function so gimp can interpret it correctly
(script-fu-register
    	"batch-create-img"	                      			;function name
    	"Batch Create Cover"                                  		;menu label
    	"Creates a image with text and sets/
	Background Color, Font Size etc Position"              		;description
    	"justme.marius"                             			;author
    	"copyleft" 							;copyright notice
    	"September 24, 2023"                          			;date created
    	""                                      			;image type that the script works on
    	SF-STRING      "Text"          "Text Box"   			;a string variable
    	SF-FONT        "Font"          "Charter"    			;a font variable
    	SF-ADJUSTMENT  "Font size"     '(50 1 1000 1 10 0 1)
        			                                        ;a spin-button
    	SF-COLOR       "Color"         '(0 0 0) 			;color variable
  )

(script-fu-menu-register "batch-create-img" "<Image>/File/Create/Text")
      
