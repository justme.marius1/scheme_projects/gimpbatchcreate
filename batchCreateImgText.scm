(define (create-image-with-text width height text)
  (let* (
         ; Create a new image
         (image (car (gimp-image-new width height RGB)))
         
         ; Create a new layer for the background
         (background-layer (car (gimp-layer-new image width height RGB "Background" 100 "NORMAL")))
         
         ; Define the font settings
         (font "Sans")
         (font-size 100)
         (font-color (list (random 255) (random 255) (random 255))) ; Random RGB color
         
         ; Create a new layer for the text
         (text-layer (car (gimp-text-layer-new image 0 0 text font-size 0 TRUE 0 font)))
  )
    ; Set the background layer's color
    (gimp-drawable-fill background-layer BG-BACKGROUND-FILL)

    ; Add layers to the image
    (gimp-image-insert-layer image background-layer 0 0)
    (gimp-image-insert-layer image text-layer 0 0)
    
    ; Set the text layer's color and position it in the center
    (gimp-text-layer-set-color text-layer font-color)
    (gimp-text-layer-set-font-size text-layer font-size)
    (gimp-text-layer-set-text text-layer text)
    (gimp-text-layer-set-horizontal-alignment text-layer 0) ; Center
    (gimp-text-layer-set-vertical-alignment text-layer 0) ; Middle

    ; Save the image with a unique filename
    (let* (
           (output-filename (string-append "dailyDiary_" (number->string (random 10000)) ".png"))
           (output-path (string-append "~/OUTPUT/PATH/HERE/" output-filename))
    )
      (file-png-save RUN-NONINTERACTIVE image (car (gimp-image-get-active-layer image)) output-path output-path 0 9 0 0 0 0 0)
    )
    (gimp-image-delete image)
  )
)

; Number of iterations
(define iterations 5)

; Generate images with text and save them
(do ((i 0 (+ i 1)))
    ((= i iterations))
  (create-image-with-text 1524 1524 (string-append "#dailyDiary_x_x_x_x" (number->string i))))

(gimp-quit 0)




(script-fu-register
    "create-image-with-text" ; Function name
    "Create Image with Text" ; Menu label
    "Description of my script" ; Description
    "Your Name" ; Author
    "Copyright (C) Your Year" ; Copyright notice
    "2023-09-25" ; Date
    "" ; Menu path
    SF-VALUE "Width" "1524" ; Input parameters
    SF-VALUE "Height" "1524"
    SF-VALUE "Iterations" "5"
)

(script-fu-menu-register "my-script" "<Image>/Filters/My Scripts")
