(define (my-script-func width height iterations)
  ; Your script logic here
  (display "Script executed successfully")
)

(script-fu-register
    "my-script-func" ; Function name
    "My Script" ; Menu label
    "Description of my script" ; Description
    "Your Name" ; Author
    "Copyright (C) Your Year" ; Copyright notice
    "2023-09-25" ; Date
    "" ; Menu path
    SF-VALUE "Width" "1524" ; Input parameters
    SF-VALUE "Height" "1524"
    SF-VALUE "Iterations" "5"
)

(script-fu-menu-register "my-script-func" "<Image>/Filters/My Scripts")
