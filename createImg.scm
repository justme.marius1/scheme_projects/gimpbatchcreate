(define (gimp-create-custom-img width height)
  	
	(let*	(
         		; Create a new image
         		(image 
				(car 
					(gimp-image-new width height RGB)
				)
			)
			
			;iteration calculation
			(iteration
				(+
					(*
					25
					365 ;dailydiary
					)
					6 ;schalttage
					25 ;Jahresrückblicke
					(*
					25
					12 ;monatsrückblicke
					)
					(*
						(*
						25
						12
						)
						4 ;wochenrückblicke	
					)
				)
			)
			
			; Create a new layer for the background
         		(background-layer 
				(car 
					(gimp-layer-new image width height RGB "Background" 100 0)
				)
			)
		
			; set a random background color
			(background-color 
				(list 
					(random 255) 
					(random 255) 
					(random 255)
				)
			)
			
			
			; font settings
         		(fontname "Noto Serif Malayalam Thin")
         		(fontsize 100)
			(datefontsize 62)
         		(font-color  ;Random RGB color
				(list 
					(random 255) ;(car(car(background-color))) 
					(random 255) 
					(random 255)
				)
			)
         		(text "#dailyDiary_7_4_8_16")
         		(date "12.07.1998")
			
			
			; Create a new layer for the titel
			(titel-layer
				(car
					(gimp-text-layer-new
						image
						text
						fontname
						fontsize
						PIXELS
					)
				)
			)
			; Create a new layer for the date
			(date-layer
				(car
					(gimp-text-layer-new
						image
						date
						fontname
						datefontsize
						PIXELS
					)
				)
			)

			;Textpositionioning
			(titelOffy	
				(-
					(/ (car(gimp-image-height image)) 2)
					(/ (car(gimp-drawable-height titel-layer)) 2)
				 )
			)
			(titelOffx 	
				(- 
					(/ (car(gimp-image-width image)) 2) 
					(/ (car(gimp-drawable-width titel-layer)) 2) 
				)  
			)
			
			(dateOffy
				(-
					(car(gimp-image-height image)) 
					(car(gimp-drawable-height date-layer))
					40
				)
			)		
			(dateOffx
				(-
					(car(gimp-image-width image))
					(car(gimp-drawable-width date-layer))
					50
				)	
			)

	
			(output-path "/OUTPUT/PATH/HERE")
			

		)


		
		; Set the back and foreground
		(gimp-context-set-background background-color)		
    		(gimp-context-set-foreground font-color)
	
		; Add the layers to the image
    		(gimp-image-insert-layer image background-layer 0 1)				
		(gimp-image-insert-layer image titel-layer 0 -1)
		(gimp-image-insert-layer image date-layer 0 -1)		

		; Set the colors for the layers
		(gimp-drawable-fill background-layer BACKGROUND-FILL)
		(gimp-text-layer-set-color titel-layer font-color)
		(gimp-text-layer-set-color date-layer font-color)		
		
		;remove lineheight
		;(gimp-text-layer-set-line-spacing date-layer 0)
		
		;Possition the text-layers
		(gimp-layer-set-offsets titel-layer titelOffx titelOffy)	
		(gimp-layer-set-offsets date-layer dateOffx dateOffy)
    		

	




		; Save the image, including all layers, as PNG    		

  		(let*	(
				(drawable 
					(car 
						(gimp-image-flatten image)
					)
				)
			)
			(file-png-save RUN-NONINTERACTIVE image drawable output-path output-path 0 9 0 0 0 0 0)	
		)
		(gimp-image-delete image)
	)
)


















(script-fu-register
   	"gimp-create-custom-img" ; Function name
   	"Create Custom Image" ; Menu label
   	"Create a bunch of Images varying in Text, Backgroundcolor" ; Description
   	"justme.marius" ; Author
   	"" ; Copyright notice
   	"2023-09-25" ; Date
   	"" ; Menu path
   	SF-VALUE "Width" "1524" ; Input parameters
  	SF-VALUE "Height" "1524"
;;;;SF-VALUE "Iterations" "5"
)

(script-fu-menu-register "gimp-create-custom-img" "<Image>/Filters/My Scripts")
